function oddEven(num) {
  if (num % 2 === 0) {
    return "even";
  } else {
    return "odd";
  }
}

oddEven(2);
oddEven(5);
